#include <iostream>
#include "WordRec.h"
#include <iomanip>
#include <string>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
using namespace std;

  // WordRec Constructor
	WordRec::WordRec(string word,int times) {
		setWord(word);
		setCount(times);
	}
	//Sets the WordRec data member word
	void WordRec::setWord(string words){
	word = words;
	}
	 //Returns WordRec data member word
	string WordRec::getWord() const {
	return (word);
	}
	//Returns WordRec data member count
	int WordRec::getCount() const {
	return (count);
	}
	  //Operator ++ overload(Pre and Post): Increments data member count
	WordRec &WordRec::operator++() {
	setCount(getCount()+1);
	return *this;
	}
	
	WordRec WordRec::operator++(int){
	WordRec temp = *this;
	setCount(count+1);
	return temp;
	}
	//Operator < overload: 
  // Returns whether a WordRec's word is alphanumerically less than another WordRec's word
	bool WordRec::operator<(const WordRec &right) const {
		if(toUpperSTR(right.word) < toUpperSTR(getWord()) )
			return false;
		else
			return true;
	}
	//Operator == overload: 
  // Returns whether a WordRec's word is equal to another WordRec's word
	bool WordRec::operator==(const WordRec &right) const {
		if ( toUpperSTR(right.word) == toUpperSTR(getWord()))
			return true;
		else
			return false;	
	}
	   //Sets the WordRec data member count
	void WordRec::setCount(int counts) {
		count = counts;
	}
	//Operator >> overload: Inputs words from file into a WordRec object
	ifstream &operator>>(ifstream &inf, WordRec &right){
	string token;
	inf >> token;
	right.setWord(token);
	return inf;
	}
	//Operator << overload: Prints a WordRec object
	ostream &operator<<(ostream &out, const WordRec &right) {
		string wWidth = right.getWord();
		int widthS = wWidth.length();
		out << right.getWord() << setw(20-widthS) << right.getCount() << endl;
		return out;
	}
	//Used to convert < and == overloaded operators to be case insensitive
	string toUpperSTR(const string& s){
		string result;
		locale loc;
    	for (unsigned int i = 0; i < s.length(); ++i)
    	{
       	 	result += toupper(s.at(i), loc);
    	}
    
    	return result;
	}