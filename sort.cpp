//CSC136
// p1.cpp
//  Author:Joshua White
//  Due: 09/10/2014
/*
 The prupose of this program is to ask the user for a file name, read the file and say how many characters and lines are in it
 and then sort the word in alphabetical order, removing repeated words.
 */
//  Created by Joshua White on 8/28/14.
//  Copyright (c) 2014 Joshua White. All rights reserved.

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>

using namespace std;

struct recordWord {
  string wordHolder;
  int count;
};

//------------Function-Prototypes---------
bool openfile(ifstream &inf,string &filename);
void countCharLines(ifstream &inf,int &numchars, int &nLine);
void wordArray( ifstream &inf,recordWord words[],int &total);
void swap(string &x, string &y);
void selSort(recordWord words[] , int &total);
int getInput(string &filename, ifstream &cfile, int &booReturns);

//----------------------------------------



//---------------------------------------
int main()
{
  recordWord words[1000];

  int numLines=0;             //Stores the number oflines in text file
  int numChars =0;             // Stores number of characters in text file
  string filename;       // user input of file name
  ifstream cfile;     // To pass an ifstream I need an ifstream
  int booReturns = 0;
  int total = 0;
  
 getInput(filename, cfile, booReturns);
 countCharLines(cfile, numChars, numLines);
  
  cout << endl << "File:" << filename << " Characters: " << numChars << "     "<< "Lines: " << numLines << endl; // Displays the number of lines and the number of characters calculated by countCharLine Function
  cfile.close();
  cfile.clear();
  cfile.open(filename.c_str());
  
  wordArray(cfile, words, total);
  
  cfile.close();
  cfile.clear();
  
  selSort( words, total); //------->
  
  return 0;
}

//---------------------Functions-----------

bool openfile( ifstream &inf, string &filename ) // Checks if the file is there returns 1 if true
{
  inf.open( filename.c_str() );
  if (!inf.is_open())
  {
    cout << "The file name: " << filename << " was not found.";
    return false;
    exit(EXIT_FAILURE);                    // if the file is not found,
  }
  cout << "File opened succesfully";
  return true;
}

void countCharLines( ifstream &inf, int &nChar, int &nLine )
{
                         // Adds 1 to the numer of lines because the first line is not denoted by a newline character
  char listchar;
  for (nLine=0; !inf.eof(); nChar++) {             // Will loop untill end of file
   listchar = inf.get();           // Gets the characters one by one untill EOF
    if (listchar == '\n'){        // Counts New line Characters to add to the line count
      nLine++;
    }
                      // adds to the number of chars until while loop reaches EOF
  }
}

void wordArray( ifstream &inf,recordWord words[],int &total){
  int x = 1;
  inf >> words[0].wordHolder;
  words[0].count++;
  
  while (x < 1000 && !inf.eof()) {   // Will loop untill end of file
    
    inf >> words[x].wordHolder;
    words[x].count = 1;
    // Gets the individual words one by one until EOF
    
    if (x >= 1000){
      cout << "This file has too many words\n";
      exit(EXIT_FAILURE);
    }
    
    x++;
    total++;
  }
}



//Funtion that swaps the wordsmin order
void swap(string &x, string &y) {
  string temp;
  temp = x;
  x = y;
  y = temp;
}

void selSort(recordWord words[] , int &total){
	string temp;
    int numtemp;
	for (int spot = 0; spot < total - 1; spot++)
    {
    
        int minIndex = spot;
        for (int index = spot + 1; index < total; index++)
        {
            if (words[index].wordHolder < words[minIndex].wordHolder)
            {
                minIndex = index;
            }
        }
        if (minIndex != spot)
        {
			swap(words[minIndex],words[spot]);
        }
        
        if (spot > 0 && words[spot].wordHolder == words[spot - 1].wordHolder)
        {
            words[spot - 1].count++;
            temp = words[total - 1].wordHolder;
            words[total - 1].wordHolder = words[spot].wordHolder;
            words[spot].wordHolder = temp;
            total--;
            spot--;
        } 
    }

    if (words[total-2].wordHolder == words[total-1].wordHolder)
    {
    total = total -1;
    words[total-1].count++; 
    }
    
    if (words[total].wordHolder == words[total-1].wordHolder)
    {
    words[total-1].count++; 
    }
   
	int reader = 0;
	while (reader != total)
	{
	cout << words[reader].wordHolder << " " << words[reader].count << endl;
	reader++;
	}

}
	
int getInput(string &filename, ifstream &cfile, int &booReturns) {
 do {                                              //Do while loop for the user input of the filename
    cout << "Please enter the name of the file you would like scanned, type exit to quit: ";
    getline(cin,filename);
    if (filename == "exit"){
      return 0;
    }                       // Gets users file name
    booReturns = openfile(cfile, filename);      // calls the function to open the file
    cout << endl;
  }
  while (booReturns == 0);
  
}

