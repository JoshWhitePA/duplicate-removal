/*
Student:Joshua White
Instructor:Dr. Daniel S. Spiegel
Class:CSC 136
Purpose: To receive file from user and manipulate the words by sorting and removing
duplicates. The user is then allowed to choose how they want to see the data displayed
they can choose the options until they decide to quit
*/

#include "WordRec.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include <string>

using namespace std;

//------------Function-Prototypes---------
void inputFile(int &total, WordRec words[]);// Groups the functions that deal with
//the file
int getInput(string &filename, ifstream &cfile, int &booReturns);//Gets the input from user of the filename
bool openfile( ifstream &inf, string &filename );//opens file & checks to see if the file is open
void swap(string &wordA, string &wordB);// Swaps words of the selSort function
void wordArray( ifstream &inf,WordRec words[],int &total);// Adds word to array 
void selSort(WordRec words[] , int &total);//Sorts word and removes duplicates
void header();//Used to display header for words and count
void findWord(int total, WordRec words[]);//Searches for a word that a user specifies
void specTime(int total,WordRec words[]);//Prints word that appears a certain number of times
void menu(int total, WordRec words[]); // Holds menu and switch statement that handle how data is displayed
void displayAll(int total,WordRec words[]);//All Words and their counts are displayed
void nChar(int total, WordRec words[]);// uses substr to display the specific num of chars user wants displayed

//----------------------------------------

int main()
{
 WordRec words[1000];
 int total = 0;
 
 inputFile(total,words);
 selSort(words, total);
 menu(total, words);
 
 return 0;
}
//----------------Functions----------------------
/*
Parameters are import export
Used to group functions that deal with the file
*/
void inputFile(int &total, WordRec words[]) {
  string filename;       // user input of file name
  ifstream cfile;     // To pass an ifstream I need an ifstream
  int booReturns = 0;
  
 getInput(filename, cfile, booReturns);
 
  cfile.close();
  cfile.clear();
  cfile.open(filename.c_str());
  wordArray(cfile, words, total);
  cfile.close();
  cfile.clear();
}

/*
Parameters are import export
Puts words from file and puts them in arrayed objects
*/
void wordArray( ifstream &inf,WordRec words[],int &total){
 total=0;
 string eCheck;
  while (total<1000 && inf >> words[total]) {
    total++; 
    }
    if (inf >> eCheck) { // checks to see if the file has more than a thousand words
    	cout << "ERROR:That file has too many words\n";
    	exit(EXIT_FAILURE);
  	}
  if (total <=0){//stops core dump when file is empty
  	cout << "\nThis file is empty, terminating application...\n";
  	exit(EXIT_FAILURE);
  }  
}

/*
Parameters are import export
Functions is used to see if file is opened successfully 
*/
bool openfile( ifstream &inf, string &filename ){ // Checks if the file is there returns 1 if true
  inf.open( filename.c_str() );
  if (!inf.is_open()){
    cout << "The file name: " << filename << " was not found.";
    return false;
    exit(EXIT_FAILURE);                    // if the file is not found,
  }
  cout << "File opened succesfully";
  return true;
}

/*
Parameters are import export
Gets input from user of the filename and allows user to exit
*/
int getInput(string &filename, ifstream &cfile, int &booReturns) {
 do {                         //Do while loop for the user input of the filename
    cout << "Please enter the name of the file you would like scanned, type exit to quit: ";
    getline(cin,filename);
    if (filename == "exit"){
      exit(EXIT_SUCCESS);
    }                       // Gets users file name
    booReturns = openfile(cfile, filename);      // calls the function to open the file
    cout << endl;
  } while (booReturns == 0);
}

/*
Both params are Import/Export
Sorts and removes duplicates from WordRec object
*/
void selSort(WordRec words[] , int &total){
	for (int spot = 0; spot < total - 1; spot++)
    {
        int minIndex = spot;
        for (int index = spot + 1; index < total; index++) {
            if (words[index] < words[minIndex]){
                minIndex = index;
            }
        }
        if (minIndex != spot){
			swap(words[minIndex],words[spot]);
        }
        
        if (spot > 0 && words[spot] == words[spot - 1]){
            words[spot - 1]++;
            swap(words[total-1],words[spot]);
            total--;
            spot--;
        } 
    }
    if (words[total-2] == words[total-1]) {//deals with duplicates found on the last word
    total = total -1;
    words[total-1]++; 
    }
}
/*
Both vars are Import/Export
used in selSort to swap positions to sort object
*/
void swap(string &wordA, string &wordB) {
  string temp;
  temp = wordA;
  wordA = wordB;
  wordB = temp;
}
/*
total & words are both import
Displays all the words and number of appearances
*/
void  displayAll(int total,WordRec words[]) {
	int idx = 0;
	header();
	while(idx != total ){
		cout << words[idx];
		idx = idx+1;
	}
}
/*
Both parameters are import
Displays a menu and contains a switch statement for the menu
it will exit when user hits Q
*/
void menu(int total,WordRec words[]) {
	char menuChoice = 'z';
	
	
	while (menuChoice != 'Q') {
	cout << "Please choose how you would like to view the data.\n";
	cout << "A)ll Words with Number of Appearances\n";
	cout << "P)rint Words That Appeared a Specified # Times\n";
	cout << "S)how first N Characters of All Words\n";
	cout << "F)ind a Word\n";
	cout << "Q)uit\n";
	cout << "Choice: ";

	cin >> menuChoice;
	menuChoice = toupper(menuChoice);
	cout << endl;

	switch (menuChoice) {
			case 'A': {
				displayAll(total, words);
			}
    			 break;
   			case 'P': {
   				specTime(total, words);
  			}   
     			break;
   			case 'S': {
   				nChar(total, words);
   			 }   
    			 break;
  			 case 'F': {
   				findWord(total, words);
   			} 
    	 		break;  
    		case 'Q': {
    			exit(EXIT_SUCCESS);
   			}   
     			break;
   
    	 	default: {
    			cout << "\n\nError: invalid choice \n\n";
    		}
		}
	}
}

/*
Both parameters are import
prints words that appeared a specific number of times
*/
void specTime(int total, WordRec words[]) {
	int numSearch = 0;
	bool found = false;
	cout << "P)rint Words That Appeared a Specified # Times\n";
	cout << "Enter the number that you would liked searched: ";
	cin >> numSearch;
	cout << endl;
	for (int idx = 0; idx <= total; idx++){//checks if the such a word exists
		if (numSearch == words[idx].getCount()){
			found = true;
		 }
	}	 
		 if (found == false)
 		cout << "There is not a word that appears " << numSearch << " times\n\n";
 			
	if (found == true){
		cout << "Words\n";
		for (int idx = 0; idx <= total; idx++){
			if (numSearch == words[idx].getCount()){
				cout << words[idx].getWord() << endl << endl;
			 }		 
		}
	}
}
/*
Both parameters are import
Preforms a case insensitive word search from user input
*/
void findWord(int total, WordRec words[]){
	string wordSearch;//User input
	bool isNull = true;//Used to flag if the word is not found
	cout << "F)ind a Word\n";
	cout << "Enter the word you would like to find: ";
	cin >> wordSearch;
	for (int idx = 0; idx < total; idx++){
			if (words[idx] == wordSearch){
				isNull = false;
			}	
	}
	if (isNull == true){
		cout << "\nThe word '" << wordSearch << "' was not found\n\n";
	}
	if (isNull == false){
		header();
	}
	for (int idxn = 0; idxn <= total; idxn++){
		if (isNull == false && words[idxn] == wordSearch){
			
			cout << words[idxn] << endl;
		}
	}
}
/*
Both parameters are import
uses substr to show the number of characters specified by user
*/
void nChar(int total, WordRec words[]){
	string wHoldT;//Could not get substr to play with the object to this and sub used get it to work
	string sub; 
	int strNum = 0;//user input
	cout << "S)how first N Characters of All Words\n";
	cout << "Enter the n you would like to find of all words: ";
	cin >> strNum;
	cout << endl;
	if (strNum != 0){// nothing will be shown if user inputs 0
	cout << "Words displayed at " << strNum << " characters\n";
	for (int idx = 0; idx <= total-1; idx++){
		wHoldT = words[idx].getWord();//turns words[idx].getWord() into something more manageable
		sub = wHoldT.substr (0,strNum);
		if (sub != ""){
			cout << sub << endl;	
			}
		}
	}
}
/*
prints header for output of lists of the object
*/
void header(){
	string header ("Words           Count");
		cout << header << endl;
}