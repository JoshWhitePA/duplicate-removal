// File: WordRec.h
// Author: Jamie Mason, updated by Dr. Spiegel
// Last Revision; Sept. 20, 2014
// A WordRec is a container used to track a word and its multiplicity in data 

#ifndef WORDREC_H
#define WORDREC_H
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

class WordRec {
  public:

  // WordRec Constructor
  WordRec(string word="",int times=1);

  //Sets the WordRec data member word
  void setWord(string words);

  //Returns WordRec data member word
  string getWord() const;
  //Returns WordRec data member count
  int getCount() const;

  //Operator ++ overload(Pre and Post): Increments data member count
  WordRec &operator++();
  WordRec operator++(int);

  //Operator < overload: 
  // Returns whether a WordRec's word is alphanumerically less than another WordRec's word
  bool operator<(const WordRec &right) const;

  //Operator == overload: 
  // Returns whether a WordRec's word is equal to another WordRec's word
  bool operator==(const WordRec &right) const;

  private:

  //Contains a word from a file
  string word;
  //Contains the multiplicity of a word from a file
  int count;

  //Sets the WordRec data member count
  void setCount(int counts);
};

//Operator << overload: Prints a WordRec object
ostream &operator<<(ostream &out, const WordRec &right);

//Operator >> overload: Inputs words from file into a WordRec object
ifstream &operator>>(ifstream &inf, WordRec &right);

//Used to convert < and == overloaded operators to be case insensitive
string toUpperSTR(const string& s);
#endif
